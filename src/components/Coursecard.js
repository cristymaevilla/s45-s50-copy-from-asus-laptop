// import {useState} from 'react';
// PropTypes-use to validate props
import PropTypes from 'prop-types'
import { Card }from 'react-bootstrap';
import {Link} from 'react-router-dom';

export default function CourseCard({courseProp}){
	// console.log(props);

	// below is used to deconstruct data from coursesData.js
	const{_id, name, description, price}=courseProp;

	/* state hook-used to keep track of info realted to indiv. component
	SYNTAX:
		const [getter,setter]= useState(initialGetterValue);*/
	// below is used to keep track of enrollees:


/*
	const [count, setCount]= useState(0);
	function enroll(){
		setCount(count + 1);
		console.log(count);
		if(count === 30){
			setCount(count);
		}
	}

	
	const [seat, setSeat]= useState(30);
	function seats(){
		setSeat(seat - 1);
		console.log(seat);
		// if(seat === 0){
		// 	alert('No more seats left.');
		// 	}
	}
function enrollSeatCount(){
	enroll();
	seats();
	if(seat === 0){
			alert('No more seats left.');
			// count(false);
			}
}*/
	
	return( 
			<Card className="mb-3">
			  <Card.Body>
			    <Card.Title>{name}</Card.Title>
			    <Card.Subtitle>Description:</Card.Subtitle>
			    <Card.Text>{description}.</Card.Text>
			    <Card.Subtitle>Price:</Card.Subtitle>
			    <Card.Text>Php {price}</Card.Text>
			    <Link className	="btn btn-primary" to={`/courses/${_id}`}>Details</Link>
			  </Card.Body>
			</Card>

		)
}

// checks the validity of proptypes
CourseCard.propTypes = {
	// shape()-if prop object conforms with the specific shape
	course:PropTypes.shape({
		name:PropTypes.string.isRequired,
		description: PropTypes.string.isRequired,
		price: PropTypes.number.isRequired
	})
}