import React from 'react';

// Context object (allows to pass info/props between components)
const UserContext = React.createContext();

// Provider -will allow other components for us to use the context object
export const UserProvider=UserContext.Provider;

export default UserContext;