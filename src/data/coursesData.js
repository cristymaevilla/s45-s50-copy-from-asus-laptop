const coursesData=[
	{
		id: 'wdC001',
		name: 'PHP-Laravel',
		description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.',
		price: 45000,
		onOffer:true
	},
	{
		id: 'wdC002',
		name: 'Phyton-Django',
		description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.',
		price: 50000,
		onOffer:true
	},
	{
		id: 'wdC003',
		name: 'Javascript-Springboot',
		description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.',
		price: 55000,
		onOffer:true
	},
]
export default coursesData;