
import {Fragment, useEffect, useState} from 'react';
import CourseCard from '../components/Coursecard'
// import coursesData from '../data/coursesData';

export default function Courses(){
	// console.log(coursesData);
	// console.log(coursesData[0]);

	// State that will be used to store the curses retrieved from the database
	const[courses, setCourses]= useState([]);
	useEffect(()=>{

		fetch('http://localhost:4000/courses/all')
		.then (res=> res.json())
		.then(data =>{
			console.log(data);

			// sets the courses state toap the data retrieved from the fetch request in several components
			setCourses(data.map(course =>{
					return(
						// key is to check the Ids in courseData
						<CourseCard key= {course.id}courseProp ={course}/>
						)
					})
				);
			
		})
	}, [])

	

	return(
		<Fragment>
			{courses}
		</Fragment>
		)
}