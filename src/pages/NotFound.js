import {useState} from 'react';
import { Button , Row , Col}from 'react-bootstrap';
import { NavLink} from 'react-router-dom';

/*export default function NotFound(){
	
		return (
			<Row>
				<Col className="p-5">	
					<h1>Page Not Found </h1>
					<p>Go back to the
					<Button variant="link" as={NavLink} to ="/" exact >HomePage</Button>
					</p>
					
				</Col>
			</Row>
			)
	
}
*/
// SIR, I used NotFound po as file name not Error.js

export default function NotFound(){

	const [isBackToHome, setIsBackToHome]= useState(false);
	function backToBanner(){
		setIsBackToHome(true);}

		return ( 
			<div>
				{isBackToHome?
					<Row>
						<Col className="p-5">	
							<h1>Zuitt Coding Bootstrap</h1>
							<p>Opportunities for everyone, everywhere</p>
							<Button variant="primary">Enroll Now!</Button>
						</Col>
					</Row>
					:

					<Col className="p-5">	
						<h1>Page Not Found </h1>
						<p>Go back to the
						<Button variant="link" onClick={backToBanner} >HomePage</Button>
						</p>
						
					</Col>

				}
			</div>
				)


}



	

			


		

	