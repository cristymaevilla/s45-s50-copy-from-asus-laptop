
import { useState, useEffect, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import { Redirect } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function Login(props){
	// to allows us to consume the User context objects and it's properties used for user validation
	const {user, setUser}=useContext(UserContext);

	/*State hooks to store the values of the input fields*/
	const[email,setEmail]=useState('');
	const [password , setPassword]=useState('');
	const[isActive, setIsActive]=useState(false);

	// console.log(email); 
	// console.log(password); 


	// Function to simulate user registration
	function authenticate(e){
		// prevents page redirection:
		e.preventDefault();


		// Fetch request to process the backend API
		/*SYNTAX :fetch ('url' ,{options}
		.then(res=> res.json())
		.then(data => {})
		*/
		fetch('http://localhost:4000/users/login', {
		    method: 'POST',
		     headers: {
		         'Content-Type': 'application/json'
		               },
		   body: JSON.stringify({
		         email: email,
		         password: password
		    })

		})
		.then(res => res.json())
		.then(data => {
			console.log(data);
				// if no user info is fuound, the "access" property will not be available and will return undefined
			if(typeof data.access !== "undefined"){
				// the token will be used to retrieve user information accross the whole frontend application and storing it in the localStorage to allow access to the user's info.
				localStorage.setItem('token', data.access);
				retrieveUserDetails(data.access);

				Swal.fire({
					title:'Login Successful',
					icon: "success",
					text:'Welcome to Zuitt!'
				})
			}
			else{
				Swal.fire({
					title:"Authentication failed",
					icon:"error",
					text:"Check your login details and try again."
				})
			}
		})
		// Set the email of the authenticated user in the local storage
		// SYNTAX: localStorage.setItem('propertyName', value);
	/*	localStorage.setItem('email',email);*/

		// set the global user state to have properted obtained from local storage
		/*setUser({
			email:localStorage.getItem('email')
		});*/

		// clear input fields after submission:
		setEmail('');
		setPassword('');

		// alert(`${email} has been verified! Welcome back!`);
		// alert('You are now logged in.')
	}

    const retrieveUserDetails = (token) => {
            fetch('http://localhost:4000/users/details', {
                headers: {
                    Authorization: `Bearer ${ token }`
                }
            })
            .then(res => res.json())
            .then(data => {
                console.log(data);

                setUser({
                    id: data._id,
                    isAdmin: data.isAdmin
                })
            })
        
}

	useEffect(()=>{
		// Validate to  enable submit button when all fields are populated and both passwords match
		if(email !== '' && password !== '')
		{
			setIsActive(true);
		}
		else{ setIsActive(false)}
	},[email, password])


	return(

		(user.id !== null) ?
		     <Redirect to="/courses" />
		 :
		 <Form onSubmit={(e) => authenticate(e)}>
		     <Form.Group controlId="userEmail">
		         <Form.Label>Email address</Form.Label>
		         <Form.Control 
		             type="email" 
		             placeholder="Enter email" 
		             value={email}
		             onChange={(e) => setEmail(e.target.value)}
		             required
		         />
		     </Form.Group>

		     <Form.Group controlId="password">
		         <Form.Label>Password</Form.Label>
		         <Form.Control 
		             type="password" 
		             placeholder="Password" 
		             value={password}
		             onChange={(e) => setPassword(e.target.value)}
		             required
		         />
		     </Form.Group>


				{/*conditionally render submit button based on isActive State; below is a ternary op.*/}
                { isActive ? 
                    <Button variant="primary" type="submit" id="submitBtn">
                        Submit
                    </Button>
                    : 
                    <Button variant="danger" type="submit" id="submitBtn" disabled>
                        Submit
                    </Button>
                }
            </Form>
        )  
}
